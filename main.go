package main

import (
	"encoding/json"
	"gitlab.com/baibolatovads/lesson-7/src/domain"
	"gitlab.com/baibolatovads/lesson-7/src/rpc/http"
	"gitlab.com/baibolatovads/lesson-7/src/user"
	"io/ioutil"
)

func main(){

	var (
		userQueryRepo    domain.UserQueryRepo
		albumQueryRepo   domain.AlbumQueryRepo
		commentQueryRepo domain.CommentQueryRepo
		photoQueryRepo   domain.PhotoQueryRepo
		postQueryRepo    domain.PostQueryRepo
		todoQueryRepo    domain.TodoQueryRepo

		userInterface    domain.UserInterface
	)

	userQueryRepo = http.NewUserQueryRepo("https://jsonplaceholder.typicode.com/users/%d/")
	albumQueryRepo = http.NewAlbumQueryRepo("https://jsonplaceholder.typicode.com/users/%d/albums")
	photoQueryRepo = http.NewPhotoQueryRepo("https://jsonplaceholder.typicode.com/albums/%d/photos")
	commentQueryRepo = http.NewCommentQueryRepo("https://jsonplaceholder.typicode.com/posts/%d/comments")
	postQueryRepo = http.NewPostQueryRepo("https://jsonplaceholder.typicode.com/users/%d/posts")
	todoQueryRepo = http.NewTodoQueryRepo("https://jsonplaceholder.typicode.com/users/%d/todos")

	userInterface = user.NewService(userQueryRepo, postQueryRepo, commentQueryRepo, albumQueryRepo, photoQueryRepo, todoQueryRepo)

	User, err := userInterface.GetUser(1)
	userData, err := json.Marshal(User)
	if err != nil {
		panic(err)
	}
	err = ioutil.WriteFile("user.json", userData, 0644)
	if err != nil {
		panic(err)
	}


}
