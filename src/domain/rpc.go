package domain

type RPC interface {
	GetUser()
	GetAlbums()
	GetComments()
	GetPhotos()
	GetPosts()
	GetTodos()
}
