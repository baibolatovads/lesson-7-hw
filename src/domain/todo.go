package domain

type Todo struct {
	UserId    int		`json:"userId"`
	id        int		`json:"id"`
	Title     string	`json:"title"`
	Completed bool		`json:"completed"`
}

type TodoInterface interface {
	GetTodos(userId int) (*[]Todo, error)
}

type TodoQueryRepo interface {
	GetTodos(userId int) (*[]Todo, error)
}


