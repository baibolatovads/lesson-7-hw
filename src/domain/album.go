package domain

type Album struct {
	UserId int    	`json:"userId"`
	Id     int    	`json:"id"`
	Title  string 	`json:"title"`
	Photos []Photo  `json:"photos, omitempty"`
}

type AlbumInterface interface {
	GetAlbums(userID int) (*[]Album, error)
}

type AlbumQueryRepo interface {
	GetAlbums(userID int) (*[]Album, error)
}