package domain

type User struct {
	UserID int
	Posts  []Post
	Albums []Album
	Todos  []Todo
}

type UserInterface interface {
	GetUser(userId int) (*User, error)
}

type UserQueryRepo interface {
	GetUser(userId int) (*User, error)
}



