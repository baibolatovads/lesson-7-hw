package domain

type Photo struct {
	AlbumId      int    `json:"albumId"`
	Id           int    `json:"id"`
	Title        string `json:"title"`
	Url          string `json:"url"`
	ThumbnailUrl string `json:"thumbnailUrl"`
}

type PhotoInterface interface {
	GetPhotos(albumId int) (*[]Photo, error)
}

type PhotoQueryRepo interface {
	GetPhotos(albumId int) (*[]Photo, error)
}

