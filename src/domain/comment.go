package domain

type Comment struct {
	PostId int 		`json:"postId"`
	Id     int 		`json:"id"`
	Name   string	`json:"name"`
	Email  string	`json:"email"`
	Body   string	`json:"body"`
}

type CommentInterface interface {
	GetComments(postId int) (*[]Comment, error)
}

type CommentQueryRepo interface {
	GetComments(postId int) (*[]Comment, error)
}

