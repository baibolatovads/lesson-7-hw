package user

import "gitlab.com/baibolatovads/lesson-7/src/domain"

type Service interface {
	GetTodos(userId int) (*[]domain.Todo, error)
}

type service struct {
	todoQueryRepo domain.TodoQueryRepo
}

func (service *service) GetTodos(userId int) (*[]domain.Todo, error) {
	todos, err := service.todoQueryRepo.GetTodos(userId)
	if err != nil {
		return nil, err
	}
	return todos, nil
}

func NewService(todoQueryRepo domain.TodoQueryRepo) Service {
	return &service{todoQueryRepo: todoQueryRepo}
}

