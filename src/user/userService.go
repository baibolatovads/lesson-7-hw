package user

import (
	"gitlab.com/baibolatovads/lesson-7/src/domain"
	"sync"
)

type Service interface {
	GetUser(userId int) (*domain.User, error)
}

func NewService(userQueryRepo domain.UserQueryRepo, postQueryRepo domain.PostQueryRepo, commentQueryRepo domain.CommentQueryRepo, albumQueryRepo domain.AlbumQueryRepo, photoQueryRepo domain.PhotoQueryRepo, todoQueryRepo domain.TodoQueryRepo) Service {
	return &service{
		userQueryRepo:    userQueryRepo,
		postQueryRepo:    postQueryRepo,
		commentQueryRepo: commentQueryRepo,
		albumQueryRepo:   albumQueryRepo,
		photoQueryRepo:   photoQueryRepo,
		todoQueryRepo:    todoQueryRepo,
	}
}

type service struct {
	userQueryRepo    domain.UserQueryRepo
	postQueryRepo    domain.PostQueryRepo
	commentQueryRepo domain.CommentQueryRepo
	albumQueryRepo   domain.AlbumQueryRepo
	photoQueryRepo   domain.PhotoQueryRepo
	todoQueryRepo    domain.TodoQueryRepo
}

func (service *service) GetUser(userId int) (*domain.User, error) {
	user, err := service.userQueryRepo.GetUser(userId)
	if err != nil {
		return nil, err
	}

	waitGroup := sync.WaitGroup{}
	errChan := make(chan error, 2)

	waitGroup.Add(1)
	go func(waitGroup *sync.WaitGroup, user *domain.User, todoQueryRepo *domain.TodoQueryRepo) {
		defer waitGroup.Done()
		var (
			todoInterface domain.TodoInterface
		)
		todoInterface = todo.NewService(*todoQueryRepo)
		todos, err := todoInterface.GetTodos(user.UserID)
		if err != nil {
			errChan <- err
			return
		}
		user.Todos = make([]domain.Todo, len(*todos))
		copy(user.Todos, *todos)
	}(&waitGroup, user, &service.todoQueryRepo)


	waitGroup.Add(1)
	go func(waitGroup *sync.WaitGroup, user *domain.User, postQueryRepo *domain.PostQueryRepo, commentQueryRepo *domain.CommentQueryRepo, errChan chan<- error) {
		var (
			postInterface domain.PostInterface
		)

		postInterface = post.NewService(*postQueryRepo, *commentQueryRepo)
		defer waitGroup.Done()
		posts, err := postInterface.GetPosts(userId)
		if err != nil {
			errChan <- err
			return
		}
		user.Posts = make([]domain.Post, len(*posts))
		copy(user.Posts, *posts)
	}(&waitGroup, user, &service.postQueryRepo, &service.commentQueryRepo, errChan)

	// extract albums concurrently
	waitGroup.Add(1)
	go func(waitGroup *sync.WaitGroup, user *domain.User, albumQueryRepo *domain.AlbumQueryRepo, photoQueryRepo *domain.PhotoQueryRepo, errChan chan<- error) {
		defer waitGroup.Done()
		var (
			albumInterface domain.AlbumInterface
		)
		albumInterface = album.NewService(*albumQueryRepo, *photoQueryRepo)
		albums, err := albumInterface.GetAlbums(user.Id)
		if err != nil {
			errChan <- err
			return
		}
		user.Albums = make([]domain.Album, len(*albums))
		copy(user.Albums, *albums)
	}(&waitGroup, user, &service.albumQueryRepo, &service.photoQueryRepo, errChan)

	waitGroup.Wait()
	close(errChan)
	errVal, errExists := <-errChan
	if errExists {
		return nil, errVal
	}
	return user, nil
}

