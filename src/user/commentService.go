package user

import "gitlab.com/baibolatovads/lesson-7/src/domain"

type Service interface {
	GetComments(postId int) (*[]domain.Comment, error)
}

type service struct {
	commentQueryRepo domain.CommentQueryRepo
}

func (service *service) GetComments(postId int) (*[]domain.Comment, error) {
	comments, err := service.commentQueryRepo.GetComments(postId)
	if err != nil {
		return nil, err
	}
	return comments, nil
}

func NewService(commentQueryRepo domain.CommentQueryRepo) Service {
	return &service{commentQueryRepo: commentQueryRepo}
}

