package user

import (
	"gitlab.com/baibolatovads/lesson-7/src/domain"
	"sync"
)

type Service interface {
	GetPosts(userId int) (*[]domain.Post, error)
}

func NewService(postQueryRepo domain.PostQueryRepo, commentQueryRepo domain.CommentQueryRepo) Service {
	return &service{
		postQueryRepo: postQueryRepo,
		commentQueryRepo: commentQueryRepo,
	}
}

type service struct {
	postQueryRepo    domain.PostQueryRepo
	commentQueryRepo domain.CommentQueryRepo
}

func (service *service) GetPosts(userId int) (*[]domain.Post, error) {
	posts, err := service.postQueryRepo.GetPosts(userId)
	if err != nil {
		return nil, err
	}

	waitGroup := sync.WaitGroup{}
	errChan := make(chan error, len(*posts))
	for i, _ := range *posts {
		waitGroup.Add(1)
		go func(waitGroup *sync.WaitGroup, post *domain.Post, commentQueryRepo *domain.CommentQueryRepo, errChan chan<- error) {
			defer waitGroup.Done()
			var (
				commentInterface domain.CommentInterface
			)

			commentInterface = comment.NewService(*commentQueryRepo)

			comments, err := commentInterface.GetComments(post.Id)
			if err != nil {
				errChan <- err
				return
			}
			post.Comments = make([]domain.Comment, len(*comments))
			copy(post.Comments, *comments)
		}(&waitGroup, &(*posts)[i], &service.commentQueryRepo, errChan)

	}
	waitGroup.Wait()
	close(errChan)
	errVal, errExists := <-errChan
	if errExists {
		return nil, errVal
	}
	return posts, nil
}

