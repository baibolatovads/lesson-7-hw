package user

import (
	"gitlab.com/baibolatovads/lesson-7/src/domain"
	"gitlab.com/baibolatovads/lesson-7/user"
	"sync"
)

type Service interface {
	GetAlbums(userId int) (*[]domain.Album, error)
}

type service struct {
	photoQueryRepo domain.PhotoQueryRepo
	albumQueryRepo domain.AlbumQueryRepo
}

func (service *service) GetAlbums(userId int) (*[]domain.Album, error) {
	albums, err := service.albumQueryRepo.GetAlbums(userId)
	if err != nil {
		return nil, err
	}

	waitGroup := sync.WaitGroup{}
	errChan := make(chan error, len(*albums))

	for i, _ := range *albums {
		waitGroup.Add(1)
		go func(waitGroup *sync.WaitGroup, album *domain.Album, photoQueryRepo *domain.PhotoQueryRepo, errChan chan<- error) {
			defer waitGroup.Done()
			var (
				photoInterface domain.PhotoInterface
			)
			photoInterface = photo.NewService(*photoQueryRepo)

			photos, err := photoInterface.GetPhotos(album.UserId)
			if err != nil {
				errChan <- err
				return
			}
			album.Photos = make([]domain.Photo, len(*photos))
			copy(album.Photos, *photos)
		}(&waitGroup, &(*albums)[i], &service.photoQueryRepo, errChan)
	}

	waitGroup.Wait()
	close(errChan)
	errVal, errExists := <-errChan
	if errExists {
		return nil, errVal
	}
	return albums, nil

}

func NewService(albumQueryRepo domain.AlbumQueryRepo, photoQueryRepo domain.PhotoQueryRepo) Service {
	return &service{
		albumQueryRepo: albumQueryRepo,
		photoQueryRepo: photoQueryRepo,
	}
}

