package user

import "gitlab.com/baibolatovads/lesson-7/src/domain"

type Service interface {
	GetPhotos(albumId int) (*[]domain.Photo, error)
}

type service struct {
	photoQueryRepo domain.PhotoQueryRepo
}

func (service *service) GetPhotos(albumId int) (*[]domain.Photo, error) {
	photos, err := service.photoQueryRepo.GetPhotos(albumId)
	if err != nil {
		return nil, err
	}
	return photos, nil
}

func NewService(photoQueryRepo domain.PhotoQueryRepo) Service {
	return &service{photoQueryRepo: photoQueryRepo}
}

