package http

import (
	"encoding/json"
	"fmt"
	"gitlab.com/baibolatovads/lesson-7/src/domain"
	"io/ioutil"
	"net/http"
)

type commentQueryRepo struct {
	baseUrl string
}

func (commentQueryRepo *commentQueryRepo) GetComments(postId int) (*[]domain.Comment, error) {
	commentsUrl := fmt.Sprintf(commentQueryRepo.baseUrl, postId)
	resp, err := http.Get(commentsUrl)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var comments []domain.Comment
	err = json.Unmarshal(body, &comments)
	if err != nil {
		return nil, err
	}
	return &comments, nil

}

func NewCommentQueryRepo(baseUrl string) domain.CommentQueryRepo {
	return &commentQueryRepo{baseUrl: baseUrl}
}

//func GetComments(url string) *[]domain.Comment{
//	res, err := http.Get(url)
//	fmt.Println("Got Data")
//	if err != nil{
//		panic(err.Error())
//	}
//	body, err := ioutil.ReadAll(res.Body)
//
//	if err != nil{
//		panic(err.Error())
//	}
//
//	commentData := &[]domain.Comment{}
//	json.Unmarshal(body, &commentData)
//	os.Exit(0)
//	return commentData
//}

