package http

import(
	"encoding/json"
	"fmt"
	"gitlab.com/baibolatovads/lesson-7/src/domain"
	"io/ioutil"
	"net/http"
)

type userQueryRepo struct {
	baseUrl string
}

func (userQueryRepo *userQueryRepo) GetUser(userId int) (*domain.User, error) {
	userUrl := fmt.Sprintf(userQueryRepo.baseUrl, userId)
	resp, err := http.Get(userUrl)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var user domain.User
	err = json.Unmarshal(body, &user)
	if err != nil {
		return nil, err
	}
	return &user, err
}

func NewUserQueryRepo(baseUrl string) domain.UserQueryRepo {
	return &userQueryRepo{baseUrl: baseUrl}
}

