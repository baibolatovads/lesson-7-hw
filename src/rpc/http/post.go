package http

import (
	"encoding/json"
	"fmt"
	"gitlab.com/baibolatovads/lesson-7/src/domain"
	"io/ioutil"
	"net/http"
)

type postQueryRepo struct {
	baseUrl string
}

func (postQueryRepo *postQueryRepo) GetPosts(userId int) (*[]domain.Post, error) {
	postUrl := fmt.Sprintf(postQueryRepo.baseUrl, userId)
	resp, err := http.Get(postUrl)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var posts []domain.Post
	err = json.Unmarshal(body, &posts)
	if err != nil {
		return nil, err
	}
	return &posts, nil

}

func NewPostQueryRepo(baseUrl string) domain.PostQueryRepo {
	return &postQueryRepo{baseUrl: baseUrl}
}

//func GetPosts(url string) *[]domain.Post{
//	res, err := http.Get(url)
//	if err != nil{
//		panic(err.Error())
//	}
//	body, err := ioutil.ReadAll(res.Body)
//
//	if err != nil{
//		panic(err.Error())
//	}
//	PostData := &[]domain.Post{}
//	json.Unmarshal(body, &PostData)
//	os.Exit(0)
//	return PostData
//}
