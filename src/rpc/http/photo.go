package http

import(
	"encoding/json"
	"fmt"
	"gitlab.com/baibolatovads/lesson-7/src/domain"
	"io/ioutil"
	"net/http"
	"os"
)

type photoQueryRepo struct {
	baseUrl string
}

func (photoQueryRepo *photoQueryRepo) GetPhotos(albumId int) (*[]domain.Photo, error) {
	photosUrl := fmt.Sprintf(photoQueryRepo.baseUrl, albumId)
	resp, err := http.Get(photosUrl)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var photos []domain.Photo
	err = json.Unmarshal(body, &photos)
	if err != nil {
		return nil, err
	}
	return &photos, nil
}

func NewPhotoQueryRepo(baseUrl string) domain.PhotoQueryRepo {
	return &photoQueryRepo{baseUrl: baseUrl}
}

//func GetPhotos(url string) *[]domain.Photo{
//	res, err := http.Get(url)
//	if err != nil{
//		panic(err.Error())
//	}
//	body, err := ioutil.ReadAll(res.Body)
//
//	if err != nil{
//		panic(err.Error())
//	}
//
//	PhotoData := &[]domain.Photo{}
//	json.Unmarshal(body, PhotoData)
//	os.Exit(0)
//	return PhotoData
//}
