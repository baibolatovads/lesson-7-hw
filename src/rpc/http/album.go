package http

import (
	"encoding/json"
	"fmt"
	"gitlab.com/baibolatovads/lesson-7/src/domain"
	"io/ioutil"
	"net/http"
)

type albumQueryRepo struct {
	baseUrl string
}


func (albumQueryRepo *albumQueryRepo) GetAlbums(userId int) (*[]domain.Album, error) {
	albumUrl := fmt.Sprintf(albumQueryRepo.baseUrl, userId)
	resp, err := http.Get(albumUrl)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var albums []domain.Album
	err = json.Unmarshal(body, &albums)
	if err != nil {
		return nil, err
	}
	return &albums, nil

}

func NewAlbumQueryRepo(baseUrl string) domain.AlbumQueryRepo {
	return &albumQueryRepo{baseUrl: baseUrl}
}

//func GetAlbums(url string) *[]domain.Album{
//	res, err := http.Get(url)
//	if err != nil{
//		panic(err.Error())
//	}
//	body, err := ioutil.ReadAll(res.Body)
//
//	if err != nil{
//		panic(err.Error())
//	}
//
//	AlbumData := &[]domain.Album{}
//	json.Unmarshal(body, &AlbumData)
//	os.Exit(0)
//	return AlbumData
//}
