package http

import (
	"encoding/json"
	"fmt"
	"gitlab.com/baibolatovads/lesson-7/src/domain"
	"io/ioutil"
	"net/http"
)

type todoQueryRepo struct {
	baseUrl string
}

func (todoQueryRepo *todoQueryRepo) GetTodos(userId int) (*[]domain.Todo, error) {
	todosUrl := fmt.Sprintf(todoQueryRepo.baseUrl, userId)
	resp, err := http.Get(todosUrl)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var todos []domain.Todo
	err = json.Unmarshal(body, &todos)
	if err != nil {
		return nil, err
	}
	return &todos, nil
}

func NewTodoQueryRepo(baseUrl string) domain.TodoQueryRepo {
	return &todoQueryRepo{baseUrl: baseUrl}
}

//func GetTodo(url string) *[]domain.Todo{
//	res, err := http.Get(url)
//	if err != nil{
//		panic(err.Error())
//	}
//	body, err := ioutil.ReadAll(res.Body)
//
//	if err != nil{
//		panic(err.Error())
//	}
//
//	TodoData := &[]domain.Todo{}
//	json.Unmarshal(body, &TodoData)
//	os.Exit(0)
//	return TodoData
//}
